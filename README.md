# lfs-test

GitLab API + LFS test repo

## 1. Getting an LFS file

```sh
curl https://gitlab.com/api/v4/projects/31930250/repository/files/test-files%2Fsmall-text.lfs.txt/raw
# returns raw file content (NO LFS transformation)
version https://git-lfs.github.com/spec/v1
oid sha256:bbae3ffcb0c86be9766c831a5ba2cc4f7704d5c1304e6783e57418e77795c01f
size 122
```

## 2. Committing a file via the GitLab API

```sh
curl --request POST 'https://gitlab.com/api/v4/projects/31930250/repository/commits' \
--header 'PRIVATE-TOKEN: <token>' \
--header 'Content-Type: application/json' \
--data-raw '{
  "branch": "main",
  "commit_message": "test lfs (raw pointer) commit via API",
  "actions": [
    {
      "action": "create",
      "file_path": "test-files/from-api.lfs.png",
      "content": "version https://git-lfs.github.com/spec/v1\noid sha256:bbae3ffcb0c86be9766c831a5ba2cc4f7704d5c1304e6783e57418e77795c01f\nsize 122\n"
    }
  ]
}'
# uploads file WITH LFS transformation
```


